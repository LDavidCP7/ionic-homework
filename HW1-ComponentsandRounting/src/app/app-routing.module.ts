import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'home/general', loadChildren: './general/general.module#GeneralPageModule' },
  { path: 'home/lists', loadChildren: './lists/lists.module#ListsPageModule' },
  { path: 'home/inputs', loadChildren: './inputs/inputs.module#InputsPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
