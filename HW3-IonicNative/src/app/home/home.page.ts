import { Component } from '@angular/core';
import { FlashlightService } from '../flashlight.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private flservice: FlashlightService) {}

  flash() {
    this.flservice.turnOn();
  }
}
