import { Injectable } from '@angular/core';
import { Flashlight } from '@ionic-native/flashlight/ngx';

@Injectable({
  providedIn: 'root'
})
export class FlashlightService {

  constructor(private flashligth: Flashlight) { }

  turnOn () {
    this.flashligth.toggle();
  }
}
