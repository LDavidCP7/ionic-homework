import { Component } from '@angular/core';
import { GithubService } from '../github.service';
import { Router } from '@angular/router';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(private ghservice: GithubService, private router: Router, private toast: ToastService) {}

  private data2;
  private usuario;

  searchUser() {
    this.ghservice.getUserInfo(this.usuario).subscribe(data => {
      this.data2 = data;
      this.ghservice.user = data;
      this.router.navigate(['/result']);
    }, err => this.toast.presentToast());
  }
}
