import { Component, OnInit } from '@angular/core';
import { GithubService } from '../github.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

  user: any;

  constructor(private ghservice: GithubService) {
    this.user = ghservice.user;
  }

  ngOnInit() {
  }

}
